from pydoc import doc
from readline import insert_text
from locust import User, task, between
from pymongo import MongoClient
from time import perf_counter
from Payload import Payload
import random
from pymongo.errors import DuplicateKeyError
import time 


class MongoUser(User):
    wait_time = between(1, 3)
    
    def __init__(self, environment):
        super().__init__(environment)
        self.client = MongoClient('mongodb://3.111.52.9:4455/', connect=False)
        self.num_keys = random.randint(50,100)
        self.len_string = random.randint(256, 512)
        self.Payload = Payload(self.num_keys, self.len_string)
        self.keys = self.Payload.get_keys()
        self.values = self.Payload.get_values()
        self.payload = self.Payload.get_payload()

    @task
    def insert_many_test(self):
        """
        This task will insert multiple documents at once. 
        """        
        db = self.client.load_test
        start = time.time()
        print(f"Size of multiple payload: {len(self.payload)}")
        try:    
            payload = Payload(self.num_keys, self.len_string)
            insert_result = db.test_many.insert_many(payload.get_payload())
            print(f"Added multiple entries of size {len(self.payload)} at once in time: {time.time() - start}\n\n")
        except DuplicateKeyError as e: 
                 print(f"Duplicate keys\n")

    @task
    def insert_one_task(self):
        """
        This task will insert one document at a time.
        """
        db = self.client.load_test
        start = time.time()
        print(f"size of payload for one at a time: {len(self.payload)}\n")
        try:   
            for document in self.payload:
                db.test_insert_one.insert_one(document)
            print(f"\nTime to insert: {time.time() - start}\n")
        except DuplicateKeyError as e: 
                print(f"Duplicate keys\n")

    @task
    def read_test(self):
        """
        1. Connect to DB
        2. Use collection in DB.
        3. read status is set to True unless a value is found.
        4. If the value of the key is found, read_status is set to False and the key, value is printed.
        5. Break and exit loop. 
        """
        db = self.client.load_test
        collection = db.test_insert_one
        random_choice = random.choice(self.keys)

        try:
            result = collection.find({}, {random_choice: 0})
            print(f"Read result: {result}\n")
        except KeyError as e:
            print(f"Key not found error: {random_choice}\n")
        
        # for document in result:
        #     keys = list(document.keys())
        #     values = list(document.values())
            # if len(keys) == 1 and len(values) == 1:
            #     print("Key not found")
            # else:
            #     print(f"Key: {keys[1]} \nvalue: {values[1]}\n")
                     
    @task 
    def update_test(self):
        """
        Use a random key and update its value. 
        """
        db = self.client.load_test
        collection = db.test_insert_one
        random_choice = random.choice(self.keys)

        try:
            update_query = {random_choice: self.Payload.get_update_sample()}
            result = collection.update_one({random_choice: 1}, {"$set":update_query})
            print(f"update result: {result}\n")
        except KeyError as e:
            print(f"Update not performed: {random_choice}")

    @task
    def delete_task(self):
        """
        Delete a random entry
        """
        db = self.client.load_test
        collection = db.test_insert_one
        random_choice = random.choice(self.keys)
        try:
            result = collection.delete_one({random_choice: 0})
            print(f"delete result: {result}\n")
        except KeyError as e:
            print(f"Key not found error: {random_choice}\n")
        


