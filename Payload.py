"""
Author: Shreyas Somashekar

This class will generate dictionary entries that will be inserted into mongoDB
The dictionaries will contain 1, 10, 100, 1000 entries. 
Each key will be unique and the values will be strings of length varying from 
100, 256, 1000 chars.
"""
import string 
import random

class Payload:
    
    def __init__(self,num_keys, len_values):
        """
        num_keys: number of keys that will be inserted into the mongoDB
        len_values: length of the string to be generated. 

        A null check will be done.
        """
        
        if num_keys <= 0:
            print("Not valid value for number of keys")
        if len_values <= 0:
            print("values length cannot be zero")
        
        self.num_keys = num_keys
        self.len_values = len_values
        self.multiple_payload = []
        self.payload = []
        self.keys = []
        self.values = []

        if len_values > 0 and num_keys > 0:
            self.generate_payload()


    def generate_payload(self):
        self.keys = self.__gen_keys()
        self.values = self.__gen_values()

        for i in range(self.num_keys):
            package = {}
            package[self.keys[i]] = self.values[i]
            self.payload.append(package)
        
        return self.payload

    
    def generate_multiple_entry_payload(self):
        pass

    def get_keys(self):
        return self.keys
    
    def get_payload(self):
        return self.payload

    def get_values(self):
        return self.values 

    def __gen_keys(self):
        keys = []
        for i in range(self.num_keys):
            keys.append(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(self.num_keys)))

        return keys

    def __gen_values(self):
        values = []

        for i in range(self.num_keys):
            values.append(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(self.len_values)))

        return values

    def get_update_sample(self):
        """
        return a single a sample value to be updated. 
        """
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(self.len_values))

