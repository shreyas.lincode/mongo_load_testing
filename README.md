# Load testing MongoDB

Basic CRUD operations on MongoDB by simulating users using locust.

## Steps 

Install locust 

```
pip install locust
```

Run 

```
locust -f mongo_load_test.py
```

Open browser at 0.0.0.0:8089

![](utils/locust.png)

Enter the users and the spawn rate.

Logs will be printed in the terminal screen.

